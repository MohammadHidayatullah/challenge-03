var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { layout: false });
});
router.get('/dashboard', function(req, res, next) {
  res.render('dashboard');
});
router.get('/cars', function(req, res, next) {
  res.render('cars');
});
router.get('/addCar', function(req, res, next) {
  res.render('addCar');
});
router.get('/editCar', function(req, res, next) {
  res.render('editCar');
});

module.exports = router;
